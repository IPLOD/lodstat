﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LODStatTool
{
    public enum DataType
    {
        Number,
        Name,
        String,
        Date
    }
}

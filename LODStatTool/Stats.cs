﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LODStatTool
{
    public static class StatsHelper
    {
        public static Stats Generate(Filter filter, IEnumerable<string> data)
        {
            Stats s = null;
            switch (filter.Type)
            {
                case DataType.String:
                    s = new StringStats();
                    break;
                case DataType.Date:
                    s = new DateStats();
                    break;
                case DataType.Number:
                    s = new FloatStats();
                    break;
            }

            if (s != null)
            {
                s.Process(filter, data);
                return s;
            }

            return null;
        }
    }

    public enum CSVHeaders
    {
        All,
        Unique,
        Min,
        Max,
        Mean,
        Stdev
    }

    public static class CSV
    {
        public static Dictionary<CSVHeaders, string> csvHeaders = new Dictionary<CSVHeaders, string>()
        {
            { CSVHeaders.All, "all"},
            {CSVHeaders.Unique, "unique"},
            {CSVHeaders.Min, "min"},
            {CSVHeaders.Max, "max"},
            {CSVHeaders.Mean, "mean"},
            {CSVHeaders.Stdev, "stdev"}
        };

        public static string Headers()
        {
            return string.Join(",", CSV.csvHeaders.Select(h => h.Value));
        }
    }

    public interface Stats
    {
        void Process(Filter filter, IEnumerable<string> data);
        string Csv();
    }

    public class StringStats : Stats
    {
        public int All { get; set; }
        public int Unique { get; set; }

        public string Csv()
        {
            return string.Join(",", CSV.csvHeaders.Select(h =>
            {
                if (h.Key == CSVHeaders.All) return All.ToString();
                else if (h.Key == CSVHeaders.Unique) return Unique.ToString();

                return "";
            }));
        }

        public void Process(Filter filter, IEnumerable<string> data)
        {
            All = data.Count();
            Unique = data.GroupBy(v => v).Select(g => g.First()).Count();
        }
    }

    public class FloatStats : Stats
    {
        public int All { get; set; }
        public int Unique { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public decimal Mean { get; set; }
        public decimal Stdev { get; set; }

        public void Process(Filter filter, IEnumerable<string> data)
        {
            var floats = data.Select(d =>
            {
                decimal date;
                if (!decimal.TryParse(d, out date)) return (decimal?)null;
                return date;
            }).Where(d => d.HasValue).Select(d => d.Value);

            All = floats.Count();
            Unique = floats.GroupBy(v => v).Select(g => g.First()).Count();
            Min = floats.Min();
            Max = floats.Max();
            Mean = floats.Sum() / floats.Count();
            Stdev = (decimal)Math.Sqrt((double)(floats.Select(f => (f - Mean) * (f - Mean)).Sum() / (floats.Count() - 1)));
        }

        public string Csv()
        {
            return string.Join(",", CSV.csvHeaders.Select(h =>
            {
                if (h.Key == CSVHeaders.All) return All.ToString();
                else if (h.Key == CSVHeaders.Unique) return Unique.ToString();
                else if (h.Key == CSVHeaders.Min) return Min.ToString();
                else if (h.Key == CSVHeaders.Max) return Max.ToString();
                else if (h.Key == CSVHeaders.Mean) return Mean.ToString();
                else if (h.Key == CSVHeaders.Stdev) return Stdev.ToString();

                return "";
            }));
        }
    }

    public class DateStats : Stats
    {
        public int All { get; set; }
        public int Unique { get; set; }
        public DateTime Min { get; set; }
        public DateTime Max { get; set; }
        public DateTime Mean { get; set; }

        public void Process(Filter filter, IEnumerable<string> data)
        {
            var dates = data.Select(d =>
            {
                DateTime date;
                if (!DateTime.TryParse(d, out date)) return (DateTime?)null;
                return date;
            }).Where(d => d.HasValue).Select(d => d.Value);

            All = dates.Count();
            Unique = dates.GroupBy(v => v).Select(g => g.First()).Count();
            Min = dates.Min();
            Max = dates.Max();
        }

        public string Csv()
        {
            return string.Join(",", CSV.csvHeaders.Select(h =>
            {
                if (h.Key == CSVHeaders.All) return All.ToString();
                else if (h.Key == CSVHeaders.Unique) return Unique.ToString();
                else if (h.Key == CSVHeaders.Min) return Min.ToString();
                else if (h.Key == CSVHeaders.Max) return Max.ToString();
                else if (h.Key == CSVHeaders.Mean) return Mean.ToString();

                return "";
            }));
        }
    }
}

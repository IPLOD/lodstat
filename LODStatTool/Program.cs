﻿using System;
using VDS.RDF.Parsing;
using CommandLine;
using CommandLine.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LODStatTool
{
    class Options
    {
        [Option('o', "output", Required = false, HelpText = "Path to output filename")]
        public string Output { get; set; }

        [Option('i', "input", Required = false, HelpText = "Paths to input filenames")]
        public IEnumerable<string> Inputs { get; set; }

        [Option('p', "predicates",  Required = false, HelpText = "Outputs a list of all predicates")]
        public bool Predicates { get; set; }


        /*
        // Omitting long name, defaults to name of property, ie "--verbose"
        [Option(
          Default = false,
          HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [Option("stdin",
          Default = false,
          HelpText = "Read from stdin")]
        public bool stdin { get; set; }

        [Value(0, MetaName = "offset", HelpText = "File offset.")]
        public long? Offset { get; set; }
        */
    }


    public struct Filter
    {
        public string RootFilter { get; set; }
        public string PredicateFilter { get; set; }
        public DataType Type { get; set; }

        public bool Predicate(string predicate)
        {
            return this.PredicateFilter == null || this.PredicateFilter == "" || predicate.Contains(this.PredicateFilter);
        }

        public bool Root(string root)
        {
            return this.RootFilter == null || this.RootFilter == "" || root.Contains(this.RootFilter);
        }
    }

    public struct Scatter
    {
        public string PredicateFilterX { get; set; }
        public string PredicateFilterY { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
              .WithParsed<Options>(opts => Run(opts));
        }

        public static void Run(Options opts)
        {
            if (opts.Output == null) opts.Output = "out";

            /*
            IEnumerable<Filter> filters = new List<Filter>()
            {
                new Filter() { PredicateFilter = "publicationNumber", Type = DataType.String },
                new Filter() { PredicateFilter = "publicationDate", Type = DataType.Date },
            };
            */

            IEnumerable<Filter> filters = new List<Filter>()
            {
                new Filter() { PredicateFilter = "patent/a", Type = DataType.Number },
                new Filter() { PredicateFilter = "patent/b", Type = DataType.Number },
            };

            IEnumerable<Scatter> scatter = new List<Scatter>()
            {
                new Scatter() { PredicateFilterX = "patent/a", PredicateFilterY = "patent/b"}
            };

            Handler hdlMy = new Handler(filters);

            NTriplesParser ntparser = new NTriplesParser();

            foreach (var input in opts.Inputs)
            {
                using (var reader = File.OpenText(input))
                {
                    ntparser.Load(hdlMy, reader);
                }

            }


            if (opts.Predicates)
            {
                File.AppendAllLines(opts.Output + ".predicates", hdlMy.dataSet.Predicates());
            }

            if (filters != null && filters.Count() > 0)
            {
                var stats = hdlMy.dataSet.Stats();

                var headers = "predicate," + CSV.Headers();
                var st_str = stats.Select(s => s.Key.Predicate + "," + s.Value.Csv());

                File.WriteAllText(opts.Output + ".csv", string.Empty);
                var lst = new List<string>() { headers };
                lst.AddRange(st_str);

                File.AppendAllLines(opts.Output + ".csv", lst);

                if (scatter != null && scatter.Count() > 0)
                {
                    foreach (var s in scatter)
                    {
                        var scatList = hdlMy.dataSet.Scatter(s);

                        var output = string.Join("", scatList.Select(k => $"({k.Item1},{k.Item2})"));

                        File.WriteAllText($"{s.PredicateFilterX.Replace("/", "_").Replace("\\", "_")}-{s.PredicateFilterY.Replace("/", "_").Replace("\\", "_")}.dat", output);
                    }
                }
            }
        }
    }
}

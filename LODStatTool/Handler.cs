﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF;
using VDS.RDF.Parsing.Handlers;

namespace LODStatTool
{
    public class PredicateWithFilter
    {
        public string Predicate { get; set; }
        public Filter Filter { get; set; }
    }

    public class RootPredicateWithFilter
    {
        public string Root { get; set; }
        public string Predicate { get; set; }
        public Filter Filter { get; set; }
    }

    public class PredicateWithFilterComparer : IEqualityComparer<PredicateWithFilter>
    {
        public bool Equals(PredicateWithFilter x, PredicateWithFilter y)
        {
            return x.Predicate.Equals(y.Predicate) &&
                (x.Filter.PredicateFilter != null && y.Filter.PredicateFilter != null ? x.Filter.PredicateFilter.Equals(y.Filter.PredicateFilter) : true) &&
                (x.Filter.RootFilter != null && y.Filter.RootFilter != null ? x.Filter.RootFilter.Equals(y.Filter.RootFilter) : true) &&
                x.Filter.Type.Equals(y.Filter.Type);
        }

        public int GetHashCode(PredicateWithFilter obj)
        {
            var tmp = obj.Predicate.GetHashCode() ^ obj.Filter.Type.GetHashCode();
            if (obj.Filter.PredicateFilter != null) tmp = tmp ^ obj.Filter.PredicateFilter.GetHashCode(); 
            if (obj.Filter.RootFilter != null) tmp = tmp ^ obj.Filter.RootFilter.GetHashCode();

            return tmp;
        }
    }

    public class DataSet
    {
        private Dictionary<PredicateWithFilter, List<string>> dataSet = new Dictionary<PredicateWithFilter, List<string>>(new PredicateWithFilterComparer());
        private Dictionary<RootPredicateWithFilter, string> triples = new Dictionary<RootPredicateWithFilter, string>();
        private HashSet<string> predicates = new HashSet<string>();

        public DataSet()
        {

        }

        public void AddPredicate(string predicate)
        {
            predicates.Add(predicate);
        }

        public void AddScatter(RootPredicateWithFilter filter, string value)
        {
            triples[filter] = value;
        }

        public void Add(PredicateWithFilter filter, string value)
        {
            if (!dataSet.ContainsKey(filter)) dataSet.Add(filter, new List<string>());

            dataSet[filter].Add(value);
        }

        public List<string> Predicates()
        {
            return predicates.ToList();
        }

        public Dictionary<PredicateWithFilter, Stats> Stats()
        {
            Dictionary<PredicateWithFilter, Stats> stats = new Dictionary<PredicateWithFilter, Stats>();

            foreach (var set in dataSet)
                stats.Add(set.Key, StatsHelper.Generate(set.Key.Filter, set.Value));

            return stats;
        }

        public IEnumerable<(decimal, decimal)> Scatter(Scatter filter)
        {
            var lst = this.triples.GroupBy(t => t.Key.Root).Select(r => new
            {
                Root = r.Key,
                ValueX = r.FirstOrDefault(k => k.Key.Predicate.Contains(filter.PredicateFilterX)),
                ValueY = r.FirstOrDefault(k => k.Key.Predicate.Contains(filter.PredicateFilterY))
            }).Where(v => v.ValueY.Value!= null && v.ValueX.Value != null);

            var scatter = lst.Select(v => (decimal.Parse(v.ValueX.Value), decimal.Parse(v.ValueY.Value)));

            return scatter;
        }
    }

    public class Handler : BaseRdfHandler
    {
        public DataSet dataSet = new DataSet();

        private readonly IEnumerable<Filter> filters;

        public Handler(IEnumerable<Filter> filters)
        {
            this.filters = filters;
        }

        public override bool AcceptsAll => throw new NotImplementedException();

        protected override bool HandleTripleInternal(Triple t)
        {
            dataSet.AddPredicate(t.Predicate.ToString());

            foreach (var filter in this.filters)
            {
                if (filter.Root(t.Subject.ToString()))
                if (filter.Predicate(t.Predicate.ToString()))
                {
                    switch (t.Object)
                    {
                        case LiteralNode lit:
                        {

                            dataSet.Add(new PredicateWithFilter { Filter = filter, Predicate = t.Predicate.ToString() }, lit.Value);

                            // add for scatter plot
                            if (filter.Type == DataType.Number)
                            {
                                dataSet.AddScatter(new RootPredicateWithFilter
                                {
                                    Filter = filter,
                                    Root = t.Subject.ToString(),
                                    Predicate = t.Predicate.ToString(),
                                }, lit.Value);
                            }

                            break;
                        }
                    }
                }
            }
            return true;
        }
    }
}
